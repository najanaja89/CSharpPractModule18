﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CsvHelper;
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters.Soap;

namespace CSharpPractModule18
{
    class Program
    {
        private static TextWriter writer;

        static void Main(string[] args)
        {
            //CsvHelper нужно подлючить через NuGet для сериализации.

            List<Person> people = new List<Person>
            {
                new Person { Name = "Ruslan", Sername = "Tyo", Age = "29"},
                new Person { Name = "John", Sername = "Doe", Age = "35"},
                 new Person{ Name = "John", Sername = "Wick", Age = "50"},
                new Person{ Name = "Jessie", Sername = "Quick", Age = "18"},
                new Person{ Name = "Bruce", Sername = "Wayne", Age = "45"},
                new Person{ Name = "Barry", Sername = "Allen", Age = "27"},
                new Person{ Name = "Walley", Sername = "West", Age = "21"},
                new Person{ Name = "Black", Sername = "Thunder", Age = "56"},
                new Person{ Name = "Roy", Sername = "Harper", Age = "28"}
            };

            while (true)
            {
                Console.WriteLine("Press 1 to start task 1");
                Console.WriteLine("Press 2 to start task 2");
                string menu = Console.ReadLine();


                switch (menu)
                {
                    case "1":
                        using (var csv = new CsvWriter(new StreamWriter("people.csv")))
                        {
                            csv.Configuration.HasHeaderRecord = true;
                            csv.Configuration.Delimiter = ";\t";
                            csv.WriteRecords(people);
                        }

                        using (var csv = new CsvReader(new StreamReader("people.csv")))
                        {
                            csv.Configuration.HasHeaderRecord = true;
                            csv.Configuration.Delimiter = ";\t";
                            var people2 = csv.GetRecords<Person>();

                            foreach (var item in people2)
                            {
                                Console.WriteLine(item.Name);
                                Console.WriteLine(item.Sername);
                                Console.WriteLine(item.Age);
                                Console.WriteLine("--------------------------------------------");
                                Console.WriteLine();
                            }
                            Console.WriteLine("Collection readed from csv");
                        }

                        SoapFormatter soapFormatter = new SoapFormatter();
                        using (FileStream fileStream = new FileStream("people.soap", FileMode.OpenOrCreate))
                        {
                            soapFormatter.Serialize(fileStream, people.ToArray());
                            Console.WriteLine("Collection saved in SOAP");
                        }

                        Console.ReadLine();
                        Console.Clear();
                        break;

                    case "2":
                        using (StreamWriter streamWriter = new StreamWriter("Json.json"))
                        {
                            streamWriter.WriteLine(JsonConvert.SerializeObject(people));
                            Console.WriteLine("Collection saved in Json");
                        }
                        break;

                    default:
                        break;
                }

            }
        }
    }
}
