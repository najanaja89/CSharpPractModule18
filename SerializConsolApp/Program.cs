﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLib;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace SerializConsolApp
{
    class Program
    {
        static void Main(string[] args)
        {
            List<PC> computers = new List<PC>
            {
                new PC ("Asus", "serial1", "Atx"),
                new PC ("HP", "serial2", "mini-Atx"),
                new PC ("Acer", "serial3", "micro-Atx"),
                new PC ("Dell", "serial4", "pico-ITX")
            };



            BinaryFormatter binaryFormatter = new BinaryFormatter();

            using (FileStream fileStream = new FileStream(@"D:\listSerial.txt", FileMode.Create))
            {
                if (File.Exists(@"D:\listSerial.txt"))
                {
                    Console.WriteLine("File will be overwritten");
                    binaryFormatter.Serialize(fileStream, computers);
                }
                else binaryFormatter.Serialize(fileStream, computers);
            }

            Console.ReadLine();
        }
    }
}
