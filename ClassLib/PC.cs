﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLib
{
    [Serializable]
    public class PC
    {
        public string Mark { get; set; }
        public string SerialNumber { get; set; }
        public string FormFactor { get; set; }

        public PC(string mark, string serialNumber, string formFactor)
        {
            Mark = mark;
            SerialNumber = serialNumber;
            FormFactor = formFactor;
        }

        public void Reboot()
        {
            Console.WriteLine("PC is rebooting");
        }

        public void PowerOff()
        {
            Console.WriteLine("PC is power off");
        }
    }
}
