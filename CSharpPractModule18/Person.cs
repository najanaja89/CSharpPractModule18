﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Soap;

namespace CSharpPractModule18
{
    [Serializable]
    public class Person
    {
        public string Name { get; set; }
        public string Sername { get; set; }
        public string Age { get; set; }
    }
}
