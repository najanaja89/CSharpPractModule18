﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using ClassLib;

namespace DeserializConsolApp
{
    class Program
    {
        static void Main(string[] args)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (FileStream fileStream = new FileStream(@"D:\listSerial.txt", FileMode.Open))
            {
                var computers = (List<PC>)binaryFormatter.Deserialize(fileStream);

                foreach (var item in computers)
                {
                    Console.WriteLine(item.Mark);
                    Console.WriteLine(item.SerialNumber);
                    Console.WriteLine(item.FormFactor);
                    Console.WriteLine("------------------------------");
                    Console.WriteLine();
                }
            }
            Console.ReadLine();
        }
    }
}
